﻿using System;

namespace ChaineCar_Console
{
    public class Program
    {

        public static bool StartMaj(string Phrase)
        {
            if (Phrase.Length == 0)
            {
                return false;
            }
            char FirstLetter = Phrase[0];
            if (FirstLetter >= 'A' && FirstLetter <= 'Z')
            {
                return true;
            }
            return false;
        }

        public static bool FinishPoint(string Phrase)
        {
            int NbCar = Phrase.Length;
            if (NbCar == 0)
            {
                return false;
            }
            char LastLetter = Phrase[NbCar - 1];
            if (LastLetter == '.' || LastLetter == '?' || LastLetter == '!' || LastLetter == ';')
            {
                return true;
            }
            return false;
        }

        static void Main(string[] args)
        {
            string Saisie;
            Console.WriteLine("Saisissez une phrase : ");
            Saisie = Console.ReadLine();
            Console.WriteLine("Vous avez saisi : {0}", Saisie);
            if (StartMaj(Saisie))
            {
                Console.WriteLine("Cette phrase commence par une majuscule.");
            }
            if (FinishPoint(Saisie))
            {
                Console.WriteLine("Cette phrase se termine par un point.");
            }
            Console.Read(); //Pause en fin de programme pour maintenir la console ouverte
        }
    }
}
