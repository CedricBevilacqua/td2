﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChaineCar_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(true, ChaineCar_Console.Program.StartMaj("Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj("les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj("les Framboises Sont Perchées Sur Le Tabouret De Mon Grand-Pere."));
            Assert.AreEqual(true, ChaineCar_Console.Program.StartMaj("LES FRAMBOISES SONT PERCHEES SUR LE TABOURET DE MON GRAND-PERE."));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj("5Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj("@Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.AreEqual(true, ChaineCar_Console.Program.StartMaj("A231(."));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj(" "));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj(""));


            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.AreEqual(false, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere"));
            Assert.AreEqual(false, ChaineCar_Console.Program.FinishPoint("Les. framboises. son.t perchées. sur. le. tabouret. de. mon. grand-pere"));
            Assert.AreEqual(false, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere,"));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere!"));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere?"));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere;"));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere..."));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("."));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("?,:!."));
            Assert.AreEqual(true, ChaineCar_Console.Program.FinishPoint("546."));
            Assert.AreEqual(false, ChaineCar_Console.Program.FinishPoint("Les framboises sont perchées sur le tabouret de mon grand-pere.5"));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj(" "));
            Assert.AreEqual(false, ChaineCar_Console.Program.StartMaj(""));
        }
    }
}
